﻿using McMaster.Extensions.CommandLineUtils;
using McMaster.Extensions.CommandLineUtils.HelpText;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdbApp.Cli
{
    public class HelpTextGenerator: DefaultHelpTextGenerator
    {
        private void CustomGenerateCommands(
            CommandLineApplication application,
            TextWriter output,
            IReadOnlyList<CommandLineApplication> visibleCommands,
            int firstColumnWidth)
        {
            if (visibleCommands.Any())
            {
                output.WriteLine();
                output.WriteLine("Commands:");
                var outputFormat = string.Format("  {{0, -{0}}}{{1}}", firstColumnWidth);

                var orderedCommands = SortCommandsByName
                    ? visibleCommands.OrderBy(c => c.Name).ToList()
                    : visibleCommands;
                foreach (var cmd in orderedCommands)
                {
                    var wrappedDescription = IndentWriter?.Write(cmd.Description);
                    var message = string.Format(outputFormat, cmd.FullName ?? cmd.Name, wrappedDescription);

                    output.Write(message);
                    output.WriteLine();
                }

                if (application.OptionHelp != null)
                {
                    output.WriteLine();
                    output.WriteLine($"Run '{application.Name} [command] --{application.OptionHelp.LongName}' for more information about a command.");
                }
            }
        }

        protected override void GenerateCommands(CommandLineApplication application, TextWriter output, IReadOnlyList<CommandLineApplication> visibleCommands, int firstColumnWidth)
        {
            var newVisibleCommands = new List<CommandLineApplication>();
            foreach (var c in visibleCommands)
            {
                if(c.Commands.Count == 0)
                    newVisibleCommands.Add(c);

                foreach (var subC in c.Commands)
                {
                    newVisibleCommands.Add(subC);
                }
            }

            CustomGenerateCommands(application, output, newVisibleCommands, firstColumnWidth + 4);
        }
    }
}
