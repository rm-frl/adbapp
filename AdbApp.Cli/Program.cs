﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using AdbApp.BLL.Commands;
using AdbApp.BLL;
using AdbApp.BLL.Services;
using StructureMap;
using MediatR;
using StructureMap.Pipeline;
using McMaster.Extensions.CommandLineUtils;
using NLog;
using NLog.Targets;
using NLog.Conditions;

namespace AdbApp.Cli
{
    class Program
    {
        static Logger logger = LogManager.GetCurrentClassLogger();
        static IMediator mediator;
        static CommandOption logOption;
        static bool IsFirstRunAdb = false;

        static void Main(string[] args)
        {
          
            BuildLogger();
            mediator = BuildMediator();

            var app = new CommandLineApplication();
            app.HelpTextGenerator = new HelpTextGenerator();
            var help = app.HelpOption(true);
            help.Description = "Показать подробную информацию";

            // create commands
            Interactive(app);
            Reinstall(app);
            Status(app);
            Download(app);
            Install(app);
            Remove(app);
            Upload(app);
            //

            logOption = app.Option("--log <on/off>", "Включить/выключить показ логов в приложении", CommandOptionType.MultipleValue, true)
                .Accepts(b => b.Values("on", "off"));

            app.OnParsingComplete(x =>
            {
                var logInput = logOption.Values.LastOrDefault();
                if(logInput != null)
                {
                    switch (logInput)
                    {
                        case "on":
                            LogManager.EnableLogging();
                            break;
                        case "off":
                            LogManager.DisableLogging();
                            break;
                    }
                }

                RunAdb();
            });

            app.OnExecute(() =>
            {
                app.Execute("interactive");
                return 1;
            });

            app.ValidationErrorHandler = (v) =>
            {
                logger.Debug(v.ErrorMessage);
                return 1;
            };

            try
            {
                app.Execute(args);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        #region commands
        static void Interactive(CommandLineApplication app)
        {
            app.Command("interactive", (i) =>
            {
                i.ShowInHelpText = false;

                i.OnExecute(() =>
                {
                    Console.WriteLine("Интерактивный режим активирован");
                    bool isrun = true;
                    while (isrun)
                    {
                        try
                        {
                            Console.Write("\r> ");
                            var input = Console.ReadLine().Split();
                            app.Execute(input);
                        }
                        catch (Exception e)
                        {
                            Show(e.Message);
                        }
                    }
                });
            });
        }

        static void Reinstall(CommandLineApplication app)
        {
            app.Command("reinstall", (i) =>
            {
                i.ShowInHelpText = false;

                i.OnExecuteAsync(async (t) =>
                {
                    Console.WriteLine("Начинается перезапись adb");
                    var res = await mediator.Send(new RunOrCheckAdbCommand(true));
                    if (res.IsFailure)
                        Console.WriteLine(res.Error);
                    else
                        Console.WriteLine("Перезапись успешно завершена");
                });
            });
        }

        static void Status(CommandLineApplication app)
        {
            app.Command("status", (i) =>
            {
                i.Description = "Узнать информацию о подключенном устройстве и последних загруженных файлах";

                i.OnExecuteAsync(async (t) =>
                {
                    var res = await mediator.Send(new GetStatusCommand());
                    Console.WriteLine(res.Value);
                });
            });
        }

        static void Download(CommandLineApplication app)
        {
            app.Command("download", (a) =>
            {
                var apk = a.Command("apk", (c) =>
                {
                    c.Description = "Скачать APK файл из сети";

                    var url = c.Argument("url", "[Опционально] Ссылка на APK файл", true);

                    c.OnExecuteAsync(async (t) =>
                    {
                        string lastInputValue = url.Values.LastOrDefault();
                        await HandleDownloadApk(lastInputValue);
                    });
                });
                apk.ShowInHelpText = true;
                apk.FullName = "download apk";

                var config = a.Command("config", (c) =>
                {
                    c.Description = "Скачать конфигурационный файл из устройства";

                    c.OnExecuteAsync(async (t) =>
                    {
                        await HandleDownloadConfig();
                    });
                });
                config.FullName = "download config";
            });
        }

        static void Install(CommandLineApplication app)
        {
            app.Command("install", (i) =>
            {
                i.Description = "Установить скаченное приложение на подключенное устройство";

                i.ExtendedHelpText = "Статус готового к установке приложения можно узнать через команду status\n" +
                "По умолчанию происходит чистая установка(обновление) приложения. Ключ --keep позволяет сохранить данные и кэш предыдущей версии";

                var keepOption = i.Option("--keep", "Сохранить данные и кэш приложения", CommandOptionType.NoValue);

                i.OnExecuteAsync(async (t) =>
                {
                    bool isKeep = keepOption.HasValue();
                    await HandleInstallApk(isKeep);
                });
            });
        }

        static void Remove(CommandLineApplication app)
        {
            app.Command("remove", (i) =>
            {
                i.Description = "Удалить приложение установленное на устройстве";

                i.ExtendedHelpText = "По умолчанию происходит полное удаление приложения. Ключ --keep позволяет сохранить данные и кэш предыдущей версии";

                var keepOption = i.Option("--keep", "Сохранить данные и кэш приложения", CommandOptionType.NoValue);

                i.OnExecuteAsync(async (t) =>
                {
                    bool isKeep = keepOption.HasValue();
                    await HandleRemoveApk(isKeep);
                });
            });
        }

        static void Upload(CommandLineApplication app)
        {
            app.Command("upload", (u) =>
            {
                u.Description = "Разместить конфигурационный файл во временную папку на устройстве";
                u.ExtendedHelpText = "Если файл уже существует, то произойдет его полная замена";

                var pOpt = u.Option("-p|--permission", "Определяет права доступа к файлу", CommandOptionType.MultipleValue);
                
                u.OnExecuteAsync(async (t) =>
                {
                    try
                    {
                        int? permission = null;
                        if(pOpt.HasValue())
                            permission = int.Parse(pOpt.Value());

                        await HandleUploadConfig(permission);
                    }
                    catch (Exception)
                    {
                        Show("Невозможно распознать права доступа к файлу");
                    }
                });
            });
        }
        #endregion

        #region handlers
        static async Task HandleDownloadApk(string url)
        {
            var cmd = url != null ? new DownloadApkCommand(ShowProgress, url) : new DownloadApkCommand(ShowProgress);

            Show("Начинается загрузка APK файла из сети...");

            await mediator
                .Send(cmd)
                .OnSuccess(d => Show($"Файл { d.Filename } успешно загружен из сети и готов к установке"))
                .OnFailure(err => { Show("Ошибка! " + err); });
        }

        static async Task HandleDownloadConfig()
        {
            var cmd = new DownloadConfigCommand();

            Show("Начинается загрузка конфигурационного файла из устройства...");

            await mediator
                .Send(cmd)
                .OnSuccess(() => Show($"Операция успешно завершена! Путь к файлу: {cmd.OutputPath}"))
                .OnFailure(err => { Show("Ошибка! " + err); });
        }

        static async Task HandleInstallApk(bool keepDataAndCache)
        {
            var cmd = new InstallApkCommand(ShowProgress, keepDataAndCache);

            Show("Начинается процедура установки приложения...");

            await mediator
                .Send(cmd)
                .OnSuccess(() => Show("Приложение успешно установлено!"))
                .OnFailure(err => { Show("Ошибка! " + err); });
        }

        static async Task HandleRemoveApk(bool keepDataAndCache, string packageName = null)
        {
            var cmd = packageName != null ? new RemoveApkCommand(keepDataAndCache, packageName) : new RemoveApkCommand(keepDataAndCache);

            Show("Начинается процедура удаления приложения...");

            await mediator
                .Send(cmd)
                .OnSuccess(() => Show("Приложение успешно удалено!"))
                .OnFailure(err => { Show("Ошибка! " + err); });
        }

        static async Task HandleUploadConfig(int? permission)
        {
            var cmd = permission != null ? new UploadConfigCommand(permission : (int)permission) : new UploadConfigCommand();

            Show("Начинается процедура размещения конфигурационного файла во временную папку на устройстве...");

            await mediator
                .Send(cmd)
                .OnSuccess(() => Show($"Операция успешно завершена! Путь к файлу: {cmd.OutputPath}"))
                .OnFailure(err => { Show("Ошибка! " + err); });
        }
        #endregion

        #region helpers
        private static void ShowProgress(int i)
        {
            if (i % 10 == 0)
                Console.Write("\r" + i + "%"); Thread.Sleep(100);
        }

        private static void Show(string message)
        {
            Console.WriteLine("\n"+message);
        }

        private static IMediator BuildMediator()
        {
            var container = new Container(cfg =>
            {
                cfg.Scan(scanner => {
                    scanner.AssembliesFromApplicationBaseDirectory();
                    scanner.AssemblyContainingType<IAndroidCommand>();
                    scanner.AddAllTypesOf(typeof(IRequestHandler<,>));
                    scanner.AddAllTypesOf(typeof(INotificationHandler<>));
                });

                cfg.For(typeof(IPipelineBehavior<,>)).Add(typeof(LoggingBehavior<,>));
                cfg.For(typeof(IPipelineBehavior<,>)).Add(typeof(DownloadCheckExtensionBehavior));
                cfg.For(typeof(IPipelineBehavior<,>)).Add(typeof(CheckAdbBehavior));
                cfg.For(typeof(IPipelineBehavior<,>)).Add(typeof(CheckDeviceBehavior));
                cfg.For(typeof(IPipelineBehavior<,>)).Add(typeof(CheckFileBehavior));

                cfg.For<IApplicationContext>().LifecycleIs<SingletonLifecycle>().Use<MemoryApplicationContext>();
                cfg.For<IAdbService>().LifecycleIs<SingletonLifecycle>().Use<AdbService>();
                cfg.For<IMediator>().LifecycleIs<TransientLifecycle>().Use<Mediator>();
                cfg.For<ServiceFactory>().Use<ServiceFactory>(ctx => ctx.GetInstance);
            });
            container.AssertConfigurationIsValid();

            return container.GetInstance<IMediator>();
        }

        private static void BuildLogger()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logconsole = new NLog.Targets.ColoredConsoleTarget("logconsole");
            var highlightRule = new ConsoleRowHighlightingRule();
            highlightRule.Condition = ConditionParser.ParseExpression("level >= LogLevel.Trace");
            highlightRule.ForegroundColor = ConsoleOutputColor.DarkGray;
            logconsole.RowHighlightingRules.Add(highlightRule);

            config.AddRule(LogLevel.Trace, LogLevel.Fatal, logconsole);
            LogManager.Configuration = config;
            LogManager.DisableLogging();
        }

        private static void RunAdb()
        {
            if (!IsFirstRunAdb)
            {
                mediator.Send(new RunOrCheckAdbCommand());
                IsFirstRunAdb = true;
            }
        }
        #endregion
    }
}
