@echo off

:: this script needs https://www.nuget.org/packages/ilmerge

:: set your target executable name (typically [projectname].exe)
SET APP_NAME=AdbApp.Console.exe

SET FINAL_APP_NAME=adbtool.exe

:: Set build, used for directory. Typically Release or Debug
SET ILMERGE_BUILD=Release

:: Set platform, typically x64
SET ILMERGE_PLATFORM=x64

:: set your NuGet ILMerge Version, this is the number from the package manager install, for example:
:: PM> Install-Package ilmerge -Version 3.0.21
:: to confirm it is installed for a given project, see the packages.config file
SET ILMERGE_VERSION=3.0.29

:: the full ILMerge should be found here:
SET ILMERGE_PATH=packages\ilmerge.%ILMERGE_VERSION%\tools\net452
:: dir "%ILMERGE_PATH%"\ILMerge.exe

echo Merging %APP_NAME% ...

:: add project DLL's starting with replacing the FirstLib with this project's DLL
"%ILMERGE_PATH%"\ILMerge.exe /ndebug AdbApp.Cli\bin\%ILMERGE_BUILD%\%APP_NAME%  ^
  /lib:AdbApp.Cli\bin\%ILMERGE_BUILD%\ ^
  /out:%FINAL_APP_NAME% ^
  AdbApp.BLL.dll ^
  CSharpFunctionalExtensions.dll ^
  ICSharpCode.SharpZipLib.dll ^
  Iteedee.ApkReader.dll ^
  McMaster.Extensions.CommandLineUtils.dll ^
  MediatR.dll ^
  NLog.dll ^
  SharpAdbClient.dll ^
  StructureMap.dll ^
  System.ValueTuple.dll ^
  
:Done
dir %FINAL_APP_NAME%