﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using ICSharpCode.SharpZipLib.Zip;
using Iteedee.ApkReader;
using SharpAdbClient;
using SharpAdbClient.DeviceCommands;

namespace AdbApp.BLL.Services
{
    public interface IAdbService
    {
        IAdbClient Client { get; }
        IAdbServer Server { get; }
        DeviceData DeviceData { get; }
        Task<Result> InstallPackage(byte[] apk, string packageFilename, Action<int> progressCallback = null, CancellationToken token = default(CancellationToken));
        Task<Result> RemovePackage(string packageName, bool keepDataAndCache, CancellationToken token = default(CancellationToken));
        Task<Result<byte[]>> GetFile(string remoteFilePath, CancellationToken token = default(CancellationToken));
        Task<Result> AddOrUpdateFile(string path, string remotePath, int permission, Action<int> progressCallback = null, CancellationToken token = default(CancellationToken));
        Task<Result> AddOrUpdateFile(byte[] file, string remotePath, int permission, Action<int> progressCallback = null, CancellationToken token = default(CancellationToken));
        Task<Result> RemoveFile(string remotePath, CancellationToken token = default(CancellationToken));
        Task<Result> RestartApp(string packageName, string actvityName, CancellationToken token = default(CancellationToken));
        Task<Result<string>> GetInternalApplicationDefaultDirectory(string packageName);
        Task<Result<string>> ExtractPackageNameFromApk(byte[] apk);
        Task<Result> ExistApplicationOnDevice(string packageName);
        Task<Result> ExistFileOnDevice(string filePath);
    }

    public class AdbService : IAdbService
    {
        public IAdbClient Client => AdbClient.Instance;

        public IAdbServer Server => AdbServer.Instance;

        public DeviceData DeviceData {
            get
            {
                try
                {
                    return Client.GetDevices().FirstOrDefault();
                }
                catch(Exception e)
                {
                    return null;
                }
            }
        }

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public async Task<Result> InstallPackage(byte[] apk, string apkFilename, Action<int> progressCallback, CancellationToken token = default(CancellationToken))
        {
            try
            {
                string remoteFilePath = LinuxPath.Combine(PackageManager.TempInstallationDirectory, apkFilename);
                var progress = new GenericProgress() { Action = progressCallback };

                using (ISyncService sync = Factories.SyncServiceFactory(DeviceData))
                using (var stream = new MemoryStream(apk))
                {
                    if (Client.GetFeatureSet(DeviceData).Contains(AdbServerFeatures.Cmd)) // for newer versions of android
                    {
                        logger.Debug("Device has cmd feature");
                        Client.Install(DeviceData, stream, "-r");
                        return Result.Ok();
                    }

                    logger.Debug($"Push apk {apkFilename} to temporary folder {PackageManager.TempInstallationDirectory}");
                    sync.Push(stream, remoteFilePath, 644, DateTime.Now, progress, token);
                }

                PackageManager manager = new PackageManager(DeviceData);
                manager.InstallRemotePackage(remoteFilePath, true);
                logger.Info("Application was installed on device");

                DeviceData.ExecuteShellCommand("rm " + remoteFilePath, null);
                logger.Debug($"Apk {apkFilename} removed from temp folder {PackageManager.TempInstallationDirectory}");
                return Result.Ok();
            }
            catch (Exception e)
            {
                logger.Error(e);
                return Result.Fail(e.Message);
            }
        }

        public async Task<Result> RemovePackage(string packageName, bool keepDataAndCache, CancellationToken token = default(CancellationToken))
        {
            try
            {
                if (keepDataAndCache) // same code as in "else" only with key -k
                {
                    string cmd = $"adb shell pm uninstall -k {packageName}";
                    logger.Debug("Execute shell command: " + cmd);
                    InstallReceiver r = new InstallReceiver();
                    DeviceData.ExecuteShellCommand(Client, cmd, r);
                    if (!string.IsNullOrEmpty(r.ErrorMessage))
                    {
                        throw new PackageInstallationException(r.ErrorMessage);
                    }
                }
                else
                { 
                    PackageManager manager = new PackageManager(DeviceData);
                    manager.UninstallPackage(packageName);
                }

                logger.Info("Application was removed from device");
                return Result.Ok();
            }
            catch(Exception e)
            {
                logger.Error(e);
                return Result.Fail("Не удалось удалить приложение на устройстве. Подробности:\n" +e.Message);
            }
        }

        public async Task<Result> AddOrUpdateFile(string path, string remotePathFile, int permission, Action<int> progressCallback, CancellationToken token = default(CancellationToken))
        {
            logger.Debug($"Push file to {remotePathFile} with permission {permission}");
            try
            {
                var progress = new GenericProgress() { Action = progressCallback };

                using (ISyncService sync = Factories.SyncServiceFactory(DeviceData))
                {
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        sync.Push(stream, remotePathFile, permission, File.GetLastWriteTime(path), progress, token);
                    }
                }

                var r = new ConsoleOutputReceiver();
                DeviceData.ExecuteShellCommand($"chmod {permission} {remotePathFile}", r);

                logger.Debug("Push was sucessfull");
                return Result.Ok();
            }
            catch (Exception e)
            {
                logger.Error(e);
                return Result.Fail(e.Message);
            }
        }

        public async Task<Result> AddOrUpdateFile(byte[] file, string remotePathFile, int permission, Action<int> progressCallback, CancellationToken token = default(CancellationToken))
        {
            logger.Debug($"Push file to {remotePathFile} with permission {permission}");
            try
            {
                var progress = new GenericProgress() { Action = progressCallback };

                using (ISyncService sync = Factories.SyncServiceFactory(DeviceData))
                {
                    using (var s = new MemoryStream(file))
                    {
                        sync.Push(s, remotePathFile, permission, DateTime.Now, progress, token);
                    }
                }

                var r = new ConsoleOutputReceiver();
                DeviceData.ExecuteShellCommand($"chmod {permission} {remotePathFile}", r);

                logger.Debug("Push was sucessfull");
                return Result.Ok();
            }
            catch(Exception e)
            {
                logger.Error(e);
                return Result.Fail(e.Message);
            }
        }

        public async Task<Result> RemoveFile(string remotePath, CancellationToken token = default(CancellationToken))
        {
            try
            {
                InstallReceiver r = new InstallReceiver();
                DeviceData.ExecuteShellCommand(Client, $"rm {remotePath}", r);
                if (!string.IsNullOrEmpty(r.ErrorMessage))
                {
                    throw new InvalidOperationException(r.ErrorMessage);
                }
                return Result.Ok();
            }
            catch (Exception e)
            {
                return Result.Fail(e.Message);
            }
        }

        public async Task<Result> RestartApp(string packageName, string actvityName, CancellationToken token = default(CancellationToken))
        {
            try
            {
                InstallReceiver r = new InstallReceiver();
                DeviceData.ExecuteShellCommand(Client, $"adb shell am start -S {packageName}/.{actvityName}", r);
                if (!string.IsNullOrEmpty(r.ErrorMessage))
                {
                    throw new InvalidOperationException(r.ErrorMessage);
                }
                return Result.Ok();
            }
            catch (Exception e)
            {
                return Result.Fail(e.Message);
            }
        }

        /// <summary>
        ///Можно сделать более обобщенный метод, чтоб возвращал всю информацию об манифесте
        /// </summary>
        public async Task<Result<string>> ExtractPackageNameFromApk(byte[] apk)
        {
            try
            {
                byte[] manifestData = null;
                using (var zs = new ZipInputStream(new MemoryStream(apk)))
                {
                    using (var ms = new MemoryStream(apk))
                    {
                        ZipFile zipfile = new ZipFile(ms);
                        ZipEntry item;
                        while ((item = zs.GetNextEntry()) != null)
                        {
                            if (item.Name.ToLower() == "androidmanifest.xml")
                            {
                                manifestData = new byte[50 * 1024];
                                using (Stream strm = zipfile.GetInputStream(item))
                                {
                                    await strm.ReadAsync(manifestData, 0, manifestData.Length);
                                }
                                break;
                            }
                        }
                    }
                }

                string packagename = new ApkReader().ExtractPackageName(manifestData);
                logger.Debug($"Extracted package name : {packagename}");
                return Result.Ok(packagename);
            }
            catch(Exception e)
            {
                logger.Error(e);
                return Result.Fail<string>("Невозможно извлечь package name из apk. Подробнее: " + e.Message);
            }
        }

        public async Task<Result> ExistApplicationOnDevice(string packageName)
        {
            PackageManager pm = new PackageManager(DeviceData);
            return pm.Packages.ContainsKey(packageName) ? Result.Ok() : Result.Fail("Приложение не установлено на устройстве");
        }

        public async Task<Result<string>> GetInternalApplicationDefaultDirectory(string packageName)
        {
            return Result.Ok($"/data/data/{packageName}");
        }

        public async Task<Result> ExistFileOnDevice(string filePath)
        {
            logger.Debug("Check existing file on device");

            ConsoleOutputReceiver r = new ConsoleOutputReceiver();
            DeviceData.ExecuteShellCommand("ls " + filePath, r);
            if (r.ParsesErrors == true || r.ToString().Contains("No such"))
                return Result.Fail($"Невозможно найти файл по пути {filePath}");
            else
                return Result.Ok();

        }

        public async Task<Result<byte[]>> GetFile(string remoteFilePath, CancellationToken token = default(CancellationToken))
        {
            logger.Debug($"Pull file {remoteFilePath}");
            try
            {
                using (ISyncService sync = Factories.SyncServiceFactory(DeviceData))
                {
                    using (var s = new MemoryStream(sync.Stat(remoteFilePath).Size))
                    {
                        sync.Pull(remoteFilePath, s, null, token);

                        byte[] raw = new byte[s.Length];
                        s.Read(raw, 0, raw.Length);

                        logger.Debug("Pull was sucessfull");
                        return Result.Ok(raw);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
                return Result.Fail<byte[]>(e.Message);
            }
        }

        private class GenericProgress : IProgress<int>
        {
            public Action<int> Action { get; set; }

            public void Report(int value)
            {
                Action?.Invoke(value);
            }
        }
    }
}
