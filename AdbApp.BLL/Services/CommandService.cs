﻿using AdbApp.BLL.Commands;
using CSharpFunctionalExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdbApp.BLL.Services
{
    public interface ICommandService
    {
        Task<Result> ExecuteCommandAsync(IRequest<Result> command, CancellationToken token = default(CancellationToken));
        Task<Result> ExecuteAllCommandsAsync(IEnumerable<IRequest<Result>> commands, CancellationToken token = default(CancellationToken));
    }

    public class CommandService: ICommandService
    {
        IMediator mediator;
        IApplicationContext context;

        public CommandService(IMediator mediator, IApplicationContext context)
        {
            this.context = context;
            this.mediator = mediator;
        }

        public async Task<Result> ExecuteCommandAsync(IRequest<Result> command, CancellationToken token = default(CancellationToken))
        {
            Result result = await mediator.Send(command, token);
            return result;
        }

        public async Task<Result> ExecuteAllCommandsAsync(IEnumerable<IRequest<Result>> commands, CancellationToken token = default(CancellationToken))
        {
            foreach (var cmd in commands)
            {
                var result = await mediator.Send(cmd);

                if (result.IsFailure)
                    return result;
            }

            return Result.Ok();
        }

    }
}
