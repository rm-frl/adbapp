﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AdbApp.BLL
{
    public static class AppSettings
    {
        public const string APK_EXTENSION = "apk";

        public const string DEFAULT_APK_URL = "https://tappian.com/tappian_kiosk_mini_release.apk";

        public const string DEFAULT_CONFIG_NAME = "settings.xml";

        public const string DEFAULT_APK_PACKAGENAME = "com.tappian.kiosk";

        public const string DEFAULT_ANDROID_CONFIG_DIR = "/storage/emulated/0/Download/";

        public static string DEFAULT_WIN_TEMP_PATH = Path.GetTempPath() + "adb-tool\\";

        public static string WIN_APP_PATH = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\";

        public const string ADB_EXE = "adb.exe";

        public const string ADB_WINAPI_DLL = "AdbWinApi.dll";

        public const string ADB_WINUSBAPI_DLL = "AdbWinUsbApi.dll";

    }
}
