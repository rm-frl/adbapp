﻿using AdbApp.BLL.Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AdbApp.BLL
{
   [DataContract]
   public class DownloadInfo
   {
        [DataMember]
        public string Url { get; set; }

        private string filename;
        [DataMember]
        public string Filename { get { return Url != null ? Path.GetFileName(Url) : null; } set { filename = value; } }

        private int length;
        [DataMember]
        public int Length { get { return RawData != null ? RawData.Length : 0; } set { length = value; } }

        public byte[] RawData { get; set; }

        [DataMember]
        public DateTime LastDownloadedTime { get; set; }
    }
}
