﻿using AdbApp.BLL.Commands;
using AdbApp.BLL.Services;
using CSharpFunctionalExtensions;
using MediatR;
using SharpAdbClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace AdbApp.BLL
{
    #region android commands behavios
    public class CheckAdbBehavior: IPipelineBehavior<IRequest, Result>
    {
        IMediator mediator;
        public CheckAdbBehavior(IMediator mediator) => this.mediator = mediator;

        public async Task<Result> Handle(IRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<Result> next)
        {
            var adbResult = await mediator.Send(new RunOrCheckAdbCommand());
            if (adbResult.IsFailure)
                return adbResult;

            var result = await next();
            return result;
        }
    }

    public class CheckDeviceBehavior : IPipelineBehavior<IAndroidCommand, Result>
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        IAdbService adbService;

        public CheckDeviceBehavior(IAdbService adbService)
        {
            this.adbService = adbService;
        }

        public async Task<Result> Handle(IAndroidCommand request, CancellationToken cancellationToken, RequestHandlerDelegate<Result> next)
        {

            if (adbService.DeviceData == null)
            {
                logger.Debug("Device is not connected");
                return Result.Fail("Устройство не подключено к компьютеру");
            }

            logger.Debug($"Connected device info: Name={adbService.DeviceData.Name} / Serial = {adbService.DeviceData.Serial}");
            logger.Debug($"Device is connected");

            var result = await next();
            return result;
        }
    }

    public class CheckFileBehavior : 
        IPipelineBehavior<InstallApkCommand, Result>, 
        IPipelineBehavior<UploadConfigCommand, Result>,
        IPipelineBehavior<DownloadConfigCommand, Result>
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        IApplicationContext context;
        IAdbService adbService;
        public CheckFileBehavior(IApplicationContext context, IAdbService adbService)
        {
            this.context = context;
            this.adbService = adbService;
        }

        public async Task<Result> Handle(InstallApkCommand request, CancellationToken cancellationToken, RequestHandlerDelegate<Result> next)
        {
            logger.Debug("Check apk in application context");
            if (context.CurrentApk == null)
                return Result.Fail("Необходимо скачать APK файл");

            return await next();
        }

        public async Task<Result> Handle(UploadConfigCommand request, CancellationToken cancellationToken, RequestHandlerDelegate<Result> next)
        {
            logger.Debug("Check existing config file on desktop");

            if (!File.Exists(request.InputPath))
                return Result.Fail("Необходимо разместить конфигурационный файл в папку с приложением");

            return await next();
        }

        public async Task<Result> Handle(DownloadConfigCommand request, CancellationToken cancellationToken, RequestHandlerDelegate<Result> next)
        {
            logger.Debug("Check existing config file on device");

            var existResult = adbService.ExistFileOnDevice(request.InputPath);
            if (existResult.IsFaulted)
                return await existResult;

            return await next();
        }
    }
    #endregion

    #region other behaviors

    public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            logger.Debug($"Request: {ShowInfoObject(request)}");
            var response = await next();
            logger.Debug($"Response: {ShowInfoObject(response)}");
            return response;
        }

        private object ShowInfoObject(object o)
        {
            try
            {
                if (o is IRequest)
                {
                    string s = "{";
                    var props = o.GetType().GetProperties().Where(f => f.MemberType == MemberTypes.Property);
                    foreach (var p in props)
                    {
                        var value = p.GetValue(o);
                        if (value != null)
                            s += $"\"{p.Name}\":\"{value.ToString()}\"";
                    }
                    s += "}";
                    return s;
                }
                else if (o is IResult)
                {
                    string result = "not defined";
                    ISerializable s = (ISerializable)o;
                    DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings();
                    settings.KnownTypes = new List<Type> { typeof(DownloadInfo) };
                    settings.DateTimeFormat = new DateTimeFormat("MM/dd/yyyy HH:mm:ss");
                    DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(o.GetType(), settings);
                    using (var stream = new MemoryStream())
                    {
                        jsonFormatter.WriteObject(stream, s);

                        result = Encoding.UTF8.GetString(stream.ToArray());
                    }
                    return result;
                }
            }
            catch(Exception e)
            {
                logger.Error("Can't parse response for logger : " + e.Message);
            }
            return o.GetType().Name;
        }
    }

    public class DownloadCheckExtensionBehavior : IPipelineBehavior<DownloadCommand, Result<DownloadInfo>>
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public async Task<Result<DownloadInfo>> Handle(DownloadCommand request, CancellationToken cancellationToken, RequestHandlerDelegate<Result<DownloadInfo>> next)
        {
            logger.Debug($"Validate file extension in url : {request.Url} ");
            try
            {
                var uri = new Uri(request.Url);
                var extension = Path.GetExtension(uri.LocalPath).ToLower().Replace(".", "");
                if (request.Extension != null && extension != request.Extension.ToLower())
                {
                    return Result.Fail<DownloadInfo>("Некорректное расширение файла по ссылке");
                }
                return await next();
            }
            catch(Exception e)
            {
                logger.Error(e);
                return Result.Fail<DownloadInfo>(e.Message);
            }
        }
    }

    #endregion
}
