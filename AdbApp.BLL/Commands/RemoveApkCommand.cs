﻿using AdbApp.BLL.Services;
using CSharpFunctionalExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdbApp.BLL.Commands
{
    public class RemoveApkCommand: IRequest<Result>, IAndroidCommand
    {
        public bool KeepDataAndCache { get; }
        public string PackageName { get; }

        public RemoveApkCommand(bool keepDataAndCache, string packageName = AppSettings.DEFAULT_APK_PACKAGENAME)
        {
            KeepDataAndCache = keepDataAndCache;
            PackageName = packageName;
        }
    }

    public class RemoveApkCommandHandler : IRequestHandler<RemoveApkCommand, Result>
    {

        IAdbService adbService;
        IApplicationContext context;

        public RemoveApkCommandHandler(IAdbService adbService, IApplicationContext context)
        {
            this.adbService = adbService;
            this.context = context;
        }

        public async Task<Result> Handle(RemoveApkCommand request, CancellationToken t)
        {
            string packageName = request.PackageName;
            if(packageName == null)
            {
                if (context.CurrentApk == null)
                    return Result.Fail("Невозможно установить системное название удаляемого приложения");

                var packageNameResult = await adbService.ExtractPackageNameFromApk(context.CurrentApk.RawData);

                if (packageNameResult.IsFailure)
                    return Result.Fail(packageNameResult.Error);

                packageName = packageNameResult.Value;
            }

            return await adbService.RemovePackage(packageName, request.KeepDataAndCache, t);
        }
    }
}
