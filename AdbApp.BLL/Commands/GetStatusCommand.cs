﻿using AdbApp.BLL.Services;
using CSharpFunctionalExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdbApp.BLL.Commands
{
    public class GetStatusCommand: IRequest<Result<string>>
    {
    }

    public class GetStatusCommandHandler : IRequestHandler<GetStatusCommand, Result<string>>
    {
        IApplicationContext context;
        IAdbService adbService;

        public GetStatusCommandHandler(IApplicationContext context, IAdbService adbService)
        {
            this.context = context;
            this.adbService = adbService;
        }

        public async Task<Result<string>> Handle(GetStatusCommand request, CancellationToken t)
        {
            return Result.Ok($@"
-----------------------
{GetAdbInfo()}
-----------------------
{GetDeviceInfo()}
-----------------------
{GetApkInfo()}
-----------------------
");
        }

        string GetAdbInfo()
        {
            string status = !adbService.Server.GetStatus().IsRunning ? "Не активен" : "Активен";
            string result =
$@"Процесс adb.exe

Статус : {status}";
            return result;
        }

        string GetDeviceInfo()
        {
            string result =
@"Подключенное устройство

";
            if (adbService.DeviceData == null)
                result +=
"Статус: Не обнаружено. Необходимо подключить";
            else
            {
                var a = adbService.DeviceData;
                result +=
$@"Модель: {a.Model}
Наименование: {a.Name}
Серийный номер: {a.Serial}
Статус: {a.State}";
            }

            return result;
        }

        string GetApkInfo()
        {
            string result =
@"APK файл

";
            if (context.CurrentApk == null)
                result +=
"Статус: Необнаружен. Необходимо скачать";
            else
            {
                var a = context.CurrentApk;
                result =
$@"
Имя файла: {a.Filename}
Размер: {a.RawData.Length} байт
Дата скачивания: {a.LastDownloadedTime.ToString("MM/dd/yyyy HH:mm:ss")}
Url: {a.Url}
Статус: Готов к установке";
            }
            return result;
        }

        string GetConfigInfo()
        {
            string result =
@"Конфигурационный файл

";
            return result;
        }
    }
}
