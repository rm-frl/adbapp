﻿using AdbApp.BLL.Services;
using CSharpFunctionalExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdbApp.BLL.Commands
{
    public class RunOrCheckAdbCommand: IRequest<Result>
    {
        public bool IsForceCreate { get; set; }

        public RunOrCheckAdbCommand(bool isForceCreate = false)
        {
            IsForceCreate = isForceCreate;
        }
    }

    public class RunOrCheckAdbCommandHandler : IRequestHandler<RunOrCheckAdbCommand, Result>
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        IAdbService adbService;

        public RunOrCheckAdbCommandHandler(IAdbService adbService)
        {
            this.adbService = adbService;
        }

        public async Task<Result> Handle(RunOrCheckAdbCommand request, CancellationToken cancellationToken)
        {
            logger.Debug("Check existing adb files");

            string adbPath = GetOrCreateAdbInTempPath(request.IsForceCreate);

            logger.Debug("Adb path : " + adbPath ?? "null");

            if(adbPath != null)
                adbService.Server.StartServer(adbPath, true);

            if (!adbService.Server.GetStatus().IsRunning)
                return Result.Fail("Невозможно подключиться к adb");

            logger.Debug("Adb is running");
            return Result.Ok();
        }

        private string GetOrCreateAdbInTempPath(bool createForce = false)
        {
            string result = null;
            var targetDir = AppSettings.DEFAULT_WIN_TEMP_PATH;

            try
            {
                if (!Directory.Exists(targetDir))
                {
                    logger.Debug($"Creating temp dir");
                    Directory.CreateDirectory(targetDir);
                }

                logger.Debug($"Temp dir: {targetDir}");

                foreach (var file in new string[] { AppSettings.ADB_EXE, AppSettings.ADB_WINAPI_DLL, AppSettings.ADB_WINUSBAPI_DLL })
                {
                    string targetFile = targetDir + file;
                    if (!File.Exists(targetFile) || createForce)
                    {
                        logger.Debug($"Import {file} in temp dir");

                        using (FileStream fs = new FileStream(targetFile, FileMode.Create))
                        {
                            var asm = Assembly.GetAssembly(this.GetType());
                            var adbRes = asm.GetManifestResourceNames().FirstOrDefault(s => s.Contains(file));
                            using (Stream manifestResourceStream = asm.GetManifestResourceStream(adbRes))
                            {
                                if (manifestResourceStream != null)
                                {
                                    byte[] numArray = new byte[manifestResourceStream.Length];
                                    manifestResourceStream.Read(numArray, 0, numArray.Length);
                                    fs.Write(numArray, 0, numArray.Length);
                                    logger.Debug($"{file} was imported in temp dir");
                                }
                            }
                        }
                    }
                }

                result = targetDir + AppSettings.ADB_EXE;
            }
            catch(Exception e)
            {
                result = null;
                logger.Error(e);
            }
            return result;
        }
    }
}
