﻿using AdbApp.BLL.Services;
using CSharpFunctionalExtensions;
using MediatR;
using SharpAdbClient;
using SharpAdbClient.DeviceCommands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdbApp.BLL.Commands
{
    public class InstallApkCommand : IRequest<Result>, IAndroidCommand
    {
        public bool KeepDataAndCache { get; }
        public Action<int> ProgressCallback { get; }

        public InstallApkCommand(Action<int> progressCallback, bool keepDataAndCache = true)
        {
            ProgressCallback = progressCallback;
            KeepDataAndCache = keepDataAndCache;
        }
    }

    public class InstallApkCommandHandler: IRequestHandler<InstallApkCommand, Result>
    {
        static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        IApplicationContext context;
        IAdbService adbService;

        public InstallApkCommandHandler(IApplicationContext context, IAdbService adbService)
        {
            this.context = context;
            this.adbService = adbService;
        }

        public async Task<Result> Handle(InstallApkCommand command, CancellationToken token)
        {
            try
            {
                //removing not implemented as option
                logger.Info("Start removing app before install");
                var packagename = await adbService.ExtractPackageNameFromApk(context.CurrentApk.RawData);
                if (packagename.IsSuccess)
                    await adbService.RemovePackage(packagename.Value, command.KeepDataAndCache); 
                //remove before installing

                var taskInstall = adbService.InstallPackage(context.CurrentApk.RawData, context.CurrentApk.Filename, command.ProgressCallback, token);
                var resultInstall = await taskInstall;
                logger.Info($"Application ({packagename}) was installed");

                return resultInstall;
            }
            catch (Exception e)
            {
                logger.Error(e);
                return Result.Fail("Не удалось установить приложение. Подробности:\n" + e.Message);
            }
        }
    }
}
