﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdbApp.BLL.Commands
{
    /// <summary>
    /// Need inherit in all android commands that need hook request.
    /// Using in pipelines
    /// </summary>
    public interface IAndroidCommand
    {
    }
}
