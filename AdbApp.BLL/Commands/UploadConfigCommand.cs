﻿using AdbApp.BLL.Services;
using CSharpFunctionalExtensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdbApp.BLL.Commands
{
    public class UploadConfigCommand : IRequest<Result>, IAndroidCommand
    {
        public int Permission { get; }
        public string InputPath { get; }
        public string OutputPath { get; }

        public UploadConfigCommand(string inputPath = null, string outputPath = null, int permission = 660)
        {
            Permission = permission;
            InputPath = inputPath ?? AppSettings.WIN_APP_PATH + AppSettings.DEFAULT_CONFIG_NAME;
            OutputPath = outputPath ?? AppSettings.DEFAULT_ANDROID_CONFIG_DIR + Path.GetFileName(InputPath);
        }
    }

    public class UploadConfigCommandHandler : IRequestHandler<UploadConfigCommand, Result>
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        IApplicationContext context;
        IAdbService adbService;

        public UploadConfigCommandHandler(IApplicationContext context, IAdbService adbService)
        {
            this.context = context;
            this.adbService = adbService;
        }

        public async Task<Result> Handle(UploadConfigCommand command, CancellationToken token)
        {
            logger.Debug("Use standard sync method for pushing");

            return await adbService.AddOrUpdateFile(command.InputPath, command.OutputPath, command.Permission, null, token);
        }
    }
}
