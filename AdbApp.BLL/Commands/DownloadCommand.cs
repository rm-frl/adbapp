﻿using CSharpFunctionalExtensions;
using MediatR;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdbApp.BLL.Commands
{
    public class DownloadCommand: IRequest<Result<DownloadInfo>>
    {
        public string Url { get; }
        public virtual string Extension { get; }
        public Action<int> ProgressCallback { get; }

        public DownloadCommand(string url, Action<int> progressCallback = null)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;

            Url = url;
            ProgressCallback = progressCallback;
        }
    }

    public class DownloadApkCommand: DownloadCommand
    {
        public override string Extension { get; } = AppSettings.APK_EXTENSION;

        public DownloadApkCommand(Action<int> progressCallback = null, string url = AppSettings.DEFAULT_APK_URL) : base(url, progressCallback) { }
    }

    public class DownloadCommandHandler : IRequestHandler<DownloadCommand, Result<DownloadInfo>>
    {
        static Logger logger = LogManager.GetCurrentClassLogger();
        IApplicationContext context;

        public DownloadCommandHandler(IApplicationContext context)
        {
            this.context = context;
        }

        private void SaveDownloadedFileToContext(DownloadCommand request, DownloadInfo download)
        {
            if(request is DownloadApkCommand)
            {
                context.CurrentApk = download;
            }
        }

        public async Task<Result<DownloadInfo>> Handle(DownloadCommand request, CancellationToken cancellationToken)
        {
            using (var wc = new WebClient())
            {
                try
                {

                    cancellationToken.Register(() => wc.CancelAsync());

                    wc.DownloadProgressChanged += (o, e) =>
                    {
                        request.ProgressCallback?.Invoke(e.ProgressPercentage);
                    };

                    logger.Info($"Start downloading from url {request.Url}");
                    byte[] data = await wc.DownloadDataTaskAsync(new Uri(request.Url));

                    var download = new DownloadInfo()
                    {
                        RawData = data,
                        Url = request.Url,
                        LastDownloadedTime = DateTime.Now
                    };

                    logger.Info($"Downloaded file properties: Url={download.Url}, Size={download.RawData.Length} byte, Date = {download.LastDownloadedTime}");

                    SaveDownloadedFileToContext(request, download);
                    return Result.Ok(download);
                }
                catch (WebException e)
                {
                    logger.Error(e);
                    return Result.Fail<DownloadInfo>("\nСообщение: " + e.Message + "\nКод статуса: " + e.Status);
                }
                catch (Exception e)
                {
                    logger.Error(e);
                    return Result.Fail<DownloadInfo>(e.Message);
                }
            }
        }
    }
}
