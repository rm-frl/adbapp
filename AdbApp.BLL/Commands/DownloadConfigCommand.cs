﻿using AdbApp.BLL.Services;
using CSharpFunctionalExtensions;
using MediatR;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AdbApp.BLL.Commands
{
    public class DownloadConfigCommand : IRequest<Result>
    {
        public string InputPath { get; }
        public string OutputPath { get; }

        public DownloadConfigCommand(string inputPath = AppSettings.DEFAULT_ANDROID_CONFIG_DIR + AppSettings.DEFAULT_CONFIG_NAME, string outputPath = null)
        {
            InputPath = inputPath;
            OutputPath = outputPath ?? AppSettings.WIN_APP_PATH + AppSettings.DEFAULT_CONFIG_NAME;
        }
    }

    public class DownloadConfigCommandCommandHandler : IRequestHandler<DownloadConfigCommand, Result>
    {
        static Logger logger = LogManager.GetCurrentClassLogger();
        IAdbService adbService;

        public DownloadConfigCommandCommandHandler(IAdbService adbService)
        {
            this.adbService = adbService;
        }

        public async Task<Result> Handle(DownloadConfigCommand request, CancellationToken t)
        {
            Result<byte[]> res = await adbService.GetFile(request.InputPath, t);
            if (res.IsFailure)
                return res;

            byte[] raw = res.Value;

            try
            {
                using (var stream = new FileStream(request.OutputPath, FileMode.Create))
                {
                    using (var w = new BinaryWriter(stream, Encoding.UTF8))
                    {
                        w.Write(raw, 0, raw.Length);
                    }
                }

                logger.Debug($"File {request.InputPath} (Length = {raw.Length} bytes) was succesfull imported to {request.OutputPath} ");

                return Result.Ok();
            }
            catch (Exception e)
            {
                return Result.Fail(e.Message);
            }
        }
    }
}
