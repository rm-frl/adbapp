﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using AdbApp.BLL.Commands;
using AdbApp.BLL.Services;

namespace AdbApp.BLL
{
    public interface IApplicationContext
    {
        DownloadInfo CurrentApk { get; set; }
        bool IsApkReadyForInstall { get; }
    }

    public class MemoryApplicationContext : IApplicationContext
    {
        static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private DownloadInfo currentApk;
        public DownloadInfo CurrentApk
        {
            get { return currentApk; }
            set { currentApk = value; logger.Info("Set CurrentApk");  }
        }

        public bool IsApkReadyForInstall => CurrentApk != null;
    }
}
